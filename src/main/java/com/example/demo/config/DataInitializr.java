package com.example.demo.config;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent){

        List<User> users = userRepository.findAll();

        if(users.isEmpty()){
            this.createUser("Felipe Lino", "felipeslino@gmail.com", "123456");
            this.createUser("Felipe Silva", "felipeslino@outlook.com", "123456");
            this.createUser("Felipe Selestino", "felipe.lino@castgroup.com.br", "123456");
        }

    }

    public void createUser(String name, String email, String password){
        User user =  new User(name, email, password);

        userRepository.save(user);
    }
}
